import java.awt.Dimension;
import javax.swing.JFrame;

public class AffichageDiagonaleComposantsHierarchiques { 
	public static void main(String[] args) {
		JFrame fenetre=new JFrame("Affichage d’une diagonale rouge");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		DessinDiagonaleComposantsHierarchiques dessin=new DessinDiagonaleComposantsHierarchiques();
		dessin.setPreferredSize(new Dimension(600,480)); 
		fenetre.setContentPane(dessin);
		fenetre.pack();
		fenetre.setVisible(true);
	}
}