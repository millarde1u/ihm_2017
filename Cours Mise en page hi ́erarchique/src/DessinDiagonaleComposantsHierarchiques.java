import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
@SuppressWarnings("serial")
class DessinDiagonaleComposantsHierarchiques extends JPanel {
	private Color couleur;
	@SuppressWarnings("unchecked")
	public DessinDiagonaleComposantsHierarchiques() {
		couleur=Color.red;
		final JPanel zone_dessin=new JPanel() {
			public void paintComponent(Graphics g) { 
				super.paintComponent(g);
				int h=getHeight();
				int w=getWidth(); g.setColor(couleur);
				g.drawLine(0,0,w-1,h-1); 
			}
		};
		// bouton "Effacer"
		JButton b=new JButton("Effacer"); b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				zone_dessin.getGraphics().clearRect(0,0,getWidth(),getHeight());
			} 
		});
		// choix de la couleur de la diagonale
		final JComboBox c=new JComboBox(new String[] {"rouge","vert","jaune","bleu"});
		c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				switch (c.getSelectedIndex()) {
					case 0 :
						couleur=Color.red;
						break; 
					case 1 :
						couleur=Color.green;
						break; 
					case 2 :
						couleur=Color.yellow;
						break; 
					case 3 :
						couleur=Color.blue;
						break; 
					default :
						couleur=Color.black; 
				}
				repaint();
			}
		});
		// les composants sont places au-dessus de la zone de dessin
		JPanel haut=new JPanel();
		haut.add(b);
		haut.add(c);
		this.setLayout(new BorderLayout()); 
		this.add(haut,BorderLayout.NORTH); 
		this.add(zone_dessin,BorderLayout.CENTER);
	}
}