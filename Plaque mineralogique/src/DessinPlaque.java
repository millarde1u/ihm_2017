import java.awt.*;
import javax.swing.*;

public class DessinPlaque extends JPanel {
	
	private String p1, p2, n, d; 
	
	public DessinPlaque(String plaque1, String milieux, String plaque2, String dep){
		this.p1=plaque1;
		this.p2=plaque2;
		this.n=milieux;
		this.d=dep;
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Font f=new Font("Arial Narrow",Font.BOLD, 120);
		Font f_dep=new Font("Arial Narrow",Font.BOLD, 60);
		g.setColor(Color.BLACK);
		g.drawRect(1, 1, 787, 172);
		g.setColor(Color.WHITE);
		g.fillRect(2, 2, 785, 170);
		g.setColor(Color.BLUE);
		g.fillRect(5, 3, 80, 168);
		g.fillRect(706,3,80,168);
		String immat=this.p1+"-"+this.n+"-"+this.p2;
		g.setColor(Color.BLACK);
		g.setFont(f);
		g.drawString(immat, 135,130);
		g.setColor(Color.WHITE);
		g.setFont(f_dep);
		g.drawString(d,720,150);
		g.drawString("F", 30, 150);
		g.drawOval(10, 15, 70, 70);
		g.drawRect( 711,15,70,70);
		
	}
}
