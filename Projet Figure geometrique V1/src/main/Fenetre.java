package main;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import vue.*;

public class Fenetre extends JFrame {
	
	private DessinFigures dessin;

	public Fenetre(String s, int w, int h){
		super(s);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel jp=new JPanel();
		dessin=new DessinFigures();
		PanneauChoix pc=new PanneauChoix(dessin);
		jp.setLayout(new BorderLayout());
		jp.add(pc, BorderLayout.NORTH);
		jp.add(dessin,BorderLayout.CENTER);
		jp.setPreferredSize(new Dimension(w,h));
		this.setContentPane(jp);
		this.pack();
	}
	
	public static void main(String[] args){
		Fenetre f=new Fenetre("Figure Geometrique",800,600);
		f.setVisible(true);
		
	}
	
}
