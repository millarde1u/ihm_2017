import java.awt.*;

/** Classe qui represente un trace de la tortue */
public class Trace {
	
	/** Point de depart du trace */
	private Point p1;

	/** Point d'arrivee du trace */
	private Point p2;
	
	/** Couleur du trace */
	private Color couleur;
	
	/** Booleen indiquant si le crayon de la tortue est leve */
	private boolean leve;
	
	/**
	* Constructeur de la Classe
	*
	* @param p1X
	*		Abscisse du point de depart du trace
	* @param p1Y
	*		Ordonnee du point de depart du trace
	* @param p2X
	*		Abscisse du point d'arrive du trace
	* @param p2Y
	*		Ordonnee du point d'arrivee du trace
	* @param c
	*		Couleur du trace
	* @param l
	*		Booleen indiquant si le crayon de la tortue est leve
	*/
	public Trace(double p1X,double p1Y, double p2X, double p2Y, Color c, boolean l) {
		this.p1 = new Point();
		p1.setLocation(p1X, p1Y);
		this.p2 = new Point();
		p2.setLocation(p2X, p2Y);
		this.couleur = c;
		this.leve = l;
	}

	/**
	* Constructeur de la Classe
	*
	* @param pt1
	*		Point de depart du trace
	* @param pt2
	*		Point d'arrivee du trace
	* @param c
	*		Couleur du trace
	* @param l
	*		Booleen indiquant si le crayon de la tortue est leve
	*/
	public Trace(Point pt1, Point pt2, Color c, boolean l) {
		this.p1 = pt1;
		this.p2 = pt2;
		this.couleur = c;
		this.leve = l;
	}
	
	/**
	* Methode accesseur qui permet d'acceder a p1, qui est le point de depart du trace
	* @return Le point de depart du trace
	*/
	public Point getP1() {
		return this.p1;
	}

	/**
	* Methode qui permet de modifier l'attribut p1, et donc de modifier le point de depart du trace
	* @param pf
	*		Nouvelle valeur que l'on veut mettre pour le point de depart du trace
	*/
	public void setP1(Point pf) {
		this.p1 = pf;
	}

	/**
	* Methode accesseur qui permet d'acceder a p2, qui est le point d'arrivee du trace
	* @return Le point d'arrivee du trace
	*/
	public Point getP2() {
		return this.p2;
	}

	/**
	* Methode qui permet de modifier l'attribut p2, et donc de modifier le point d'arrivee du trace
	* @param ps
	*		Nouvelle valeur que l'on veut mettre pour le point d'arrivee du trace
	*/
	public void setP2(Point ps) {
		this.p2 = ps;
	}

	/**
	* Methode accesseur qui permet d'acceder a couleur, qui est la couleur du trace
	* @return La couleur du trace
	*/
	public Color getCouleur() {
		return this.couleur;
	}

	/**
	* Methode qui permet de modifier l'attribut couleur, et donc de modifier la couleur du trace
	* @param coul
	*		Nouvelle valeur que l'on veut mettre pour la couleur du trace
	*/
	public void setCouleur(Color coul) {
		this.couleur = coul;
	}

	/**
	* Methode qui retourne leve, un booleen qui permet de savoir si le crayon de la tortue est leve 
	* @return True, si et seulement si le crayon de la tortue est leve
	*/
	public boolean getLeve() {
		return this.leve;
	}

	/**
	* Methode qui permet de modifier l'attribut leve
	* @param lev
	*		Nouvelle valeur que l'on veut mettre pour le booleen leve
	*/
	public void setLeve(boolean lev) {
		this.leve = lev;
	}
	
}