import java.awt.*;

/** Classe qui represente les informations concernant la tortue */
public class Tortue {

	/** Abscisse de la position de la tortue */
	private double x;

	/** Ordonnee de la position de la tortue */
	private double y;

	/** Orientation de la tortue (en degre) */
	private int orientation;

	/** Couleur du crayon de la tortue */
	private Color couleur;

	/** Booleen indiquant si le crayon de la tortue est leve */
	private boolean leve;

	/** Booleen indiquant si la tortue est affichee */
	private boolean tAffiche;
	
	/**
	* Constructeur de la Classe
	*
	* @param x
	*		Abscisse de la position de la tortue
	* @param y
	*		Ordonnee de la position de la tortue
	* @param c
	*		Couleur du crayon de la tortue
	*/
	public Tortue(int x, int y, Color c) {
		this.x = x;
		this.y = y;
		this.couleur = c;
		this.orientation = -90;
		this.tAffiche = true;
	}
	
	/**
	* Methode qui permet d'afficher la tortue
	*/
	public void afficherTortue() {
		this.tAffiche = true;
	}
	
	/**
	* Methode qui permet de cacher la tortue
	*/
	public void cacherTortue() {
		this.tAffiche = false;
	}
	
	/**
	* Methode qui permet de baisser le crayon de la tortue
	*/
	public void baisserCrayon() {
		this.leve = false;
	}
	
	/**
	* Methode qui permet de lever le crayon de la tortue
	*/
	public void leverCrayon() {
		this.leve = true;
	}

	/**
	* Methode qui retourne leve, un booleen qui permet de savoir si le crayon de la tortue est leve
	* @return True, si et seulement si le crayon de la tortue est leve
	*/
	public boolean etreLeve() {
		return this.leve;
	}
	
	/**
	* Methode qui retourne tAffiche, un booleen qui permet de savoir si la tortue est affichee
	* @return True, si et seulement si la tortue est affichee
	*/
	public boolean etreVisible() {
		return this.tAffiche;
	} 
	
	/**
	* Methode accesseur qui permet d'acceder a orientation, qui est l'orientation de la tortue
	* @return L'orientation de la tortue
	*/
	public int getAngle() {
		return this.orientation;
	}

	/**
	* Methode qui permet de modifier l'attribut orientation, et donc de modifier l'orientation de la tortue
	* @param angle
	*		Nouvelle valeur que l'on veut mettre pour l'orientation de la tortue		
	*/
	public void setAngle(int angle) {
		this.orientation = angle;
	}
	
	/**
	* Methode accesseur qui permet d'acceder a couleur, qui est la couleur du crayon de la tortue
	* @return La couleur du crayon de la tortue
	*/
	public Color getCouleur() {
		return this.couleur;
	}
	
	/**
	* Methode qui permet de modifier l'attribut couleur, et donc de modifier la couleur du crayon de la tortue
	* @param c
	*		Nouvelle valeur que l'on veut mettre pour la couleur du crayon de la tortue		
	*/
	public void setCouleur(Color c) {
		this.couleur = c;
	}
	
	/**
	* Methode accesseur qui permet d'acceder a x, qui est l'abscisse de la position de la tortue
	* @return L'abscisse de la position de la tortue 
	*/
	public double getX() {
		return this.x;
	}
	
	/**
	* Methode accesseur qui permet d'acceder a y, qui est l'ordonnee de la position de la tortue
	* @return L'ordonnee de la position de la tortue
	*/
	public double getY() {
		return this.y;
	}
	
	/**
	* Methode qui permet de modifier les attributs x et y, et donc de changer la position de la tortue
	* @param x
	*		Nouvelle valeur que l'on veut mettre en abscisse pour la position de la tortue
	* @param y
	*		Nouvelle valeur que l'on veut mettre en ordonnee pour la position de la tortue
	*/
	public void setPosition(double x1, double y1) {
		this.x = x1;
		this.y = y1;
	}
}