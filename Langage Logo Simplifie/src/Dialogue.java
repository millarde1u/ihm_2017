import java.awt.*;
import java.util.*;

/** Classe qui implemente le dialogue avec l'utilisateur pour saisir et exécuter les instructions LOGO */
public class Dialogue {
	
	/** Attribut qui permet la gestion graphique de la tortue */
	private static SimpleLogo logo;

	/** Attribut qui permet de manipuler les informations sur la tortue */
	private static Tortue tortue;
	
	/**
	* Methode qui permet de faire avancer la tortue
	* @param a
	*		Valeur pour laquelle la tortue doit avancer
	*/
	private static void avancer(int a) {
		double b = a*Math.cos(Math.toRadians(tortue.getAngle()))+tortue.getX();
		double c = a*Math.sin(Math.toRadians(tortue.getAngle()))+tortue.getY();
		Trace t = new Trace(tortue.getX(),tortue.getY(),b,c,tortue.getCouleur(),tortue.etreLeve());
		logo.insererTrace(t);
		tortue.setPosition(b, c);
	}
	
	/**
	* Methode qui permet de faire reculer la tortue
	* @param a
	*		Valeur pour laquelle la tortue doit reculer
	*/
	private static void reculer(int a) {
		Dialogue.avancer(-a);
	}
	
	/**
	* Methode qui permet de faire tourner la tourtue a droite
	* @param a
	*		Valeur pour laquelle la tortue doit tourner a droite
	*/
	private static void TournerDroite(int a) {
		tortue.setAngle(tortue.getAngle()+a);
	}
	
	/**
	* Methode qui permet de faire tourner la tortue a gauche
	* @param a
	*		Valeur pour laquelle la tortue doit tourner a gauche
	*/
	private static void TournerGauche(int a) {
		tortue.setAngle(tortue.getAngle()-a);
	}
	
	/**
	* Methode qui permet de baisser le crayon de la tortue
	*/
	private static void baisserCrayon() {
		tortue.baisserCrayon();
	}
	
	/**
	* Methode qui permet de lever le crayon de la tortue
	*/
	private static void leverCrayon() {
		tortue.leverCrayon();
	}
	
	/**
	* Methode qui permet d'afficher la tortue
	*/
	private static void afficherTortue() {
		tortue.afficherTortue();
	}

	/**
	* Methode qui permet de cacher la tortue
	*/
	private static void cacherTortue() {
		tortue.cacherTortue();
	}
	
	/**
	* Methode qui permet de changer la couleur des traits
	*/
	
	private static void changerCouleur(String c) {
		if (c.equals("white") || c.equals("WHITE")){
			tortue.setCouleur(Color.WHITE);
		}
		if (c.equals("black") || c.equals("BLACK")){
			tortue.setCouleur(Color.BLACK);
		}
		if (c.equals("blue") || c.equals("BLUE")){
			tortue.setCouleur(Color.BLUE);
		}
		if (c.equals("red") || c.equals("RED")){
			tortue.setCouleur(Color.RED);
		}
		if (c.equals("green") || c.equals("GREEN")){
			tortue.setCouleur(Color.GREEN);
		}
	}

	/**
	* Methode qui permet d'effacer l'ecran graphique
	*/
	private static void effacer() {
		logo.initialiserTortue();
	}

	/** 
	 * Methode qui permet d'executer la bonne methode en fonction du string rentre en parametre
	 * @return True, si l'action a ete effectuee
	 */
	public static boolean executerCommande(SimpleLogo l, String s) {
		Dialogue.tortue = l.getTortue();
		Dialogue.logo = l;
		Scanner sc = new Scanner(s);
		String entree,argument;
		boolean etat = true;
		try{
			while(sc.hasNext()){
				entree = (sc.next()).toLowerCase();
				switch(entree){
				case "av" :
					argument = sc.next();
					Dialogue.avancer(Integer.parseInt(argument));
					l.repaint();
					break;
				case "re" :
					argument = sc.next();
					Dialogue.reculer(Integer.parseInt(argument));
					l.repaint();
					break;
				case "td" :
					argument = sc.next();
					Dialogue.TournerDroite(Integer.parseInt(argument));
					l.repaint();
					break;
				case "tg" :
					argument = sc.next();
					Dialogue.TournerGauche(Integer.parseInt(argument));
					l.repaint();
					break;
				case "lc" :
					Dialogue.leverCrayon();
					l.repaint();
					break;
				case "bc" :
					Dialogue.baisserCrayon();
					l.repaint();
					break;
				case "ct" :
					Dialogue.cacherTortue();
					l.repaint();
					break;
				case "mt" :
					Dialogue.afficherTortue();
					l.repaint();
					break;
				case "eff" :
					Dialogue.effacer();
					l.repaint();
					break;
				case "c" :
					argument = sc.next();
					Dialogue.changerCouleur(argument);
					l.repaint();
					break;
				default :
					etat = false;
				}
			}
		}
		catch(NumberFormatException e1){
			System.out.println("§§ Petite erreur de syntaxe §§");
		}
		catch(Exception e2){
			System.out.println("§§ Erreur inconnue §§");
			e2.printStackTrace();
		}
		sc.close();
		return etat;
	}
}