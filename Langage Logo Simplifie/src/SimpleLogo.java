import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

@SuppressWarnings("serial")

/** Classe qui gere la partie graphique du programme */
public class SimpleLogo extends JPanel {
	
	/** La tortue graphique */
	private Tortue tortue;

	/** Liste des traces */
	private ArrayList<Trace> tabTrace;
	
	@Override
	/**
	* Methode d’affichage qui dessine l’ensemble des segments stockés dans le tableau des tracés, en tenant compte de l’état du crayon et la couleur du segment
	* @param g
	*		Variable qui va contenir toute la partie graphique
	*/
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		if(tortue.etreVisible()) {
			this.placerTortue((int)tortue.getX(),(int)tortue.getY(),tortue.getAngle(),g);		
		}
		for (Trace n:tabTrace) {
			if (!n.getLeve()) {
				g.setColor(n.getCouleur());
				g.drawLine((int)n.getP1().getX(), (int)n.getP1().getY(), (int)n.getP2().getX(), (int)n.getP2().getY());
			}
		}
	}
	
	/**
	* Methode qui permet d'initialiser les attributs de la Classe
	*/
	
	public void initialiserTortue() {
		tortue = new Tortue(this.getWidth()/2,this.getHeight()/2,Color.black);
		this.tabTrace = new ArrayList<Trace>();
	}
	
	/**
	* Methode qui permet de dessiner la tortue representeepar un triangle a la position (x, y) de l'ecran et faisant un angle ang
	* @param x
	*		Abscisse de la position de la tortue
	* @param y
	*		Ordonnée de la position de la tortue
	* @param ang
	*		Orientation du crayon de la tortue
	* @param gr
	*		Contexte graphique
	*/
	public void placerTortue(int x, int y, int ang, Graphics gr) { 
		int vx[] = new int[4];
		int vy[] = new int[4];
		double angle = Math.toRadians(ang);
		vx[0] = (int)(x + 15f * Math.cos(angle)); 
		vy[0] = (int)(y + 15f * Math.sin(angle)); 
		angle += 2 * Math.PI / 3.0;
		vx[1] = (int)(x + 10f * Math.cos(angle)); 
		vy[1] = (int)(y + 10f * Math.sin(angle)); 
		angle += 2f * Math.PI / 3f;
		vx[2] = (int)(x + 10f * Math.cos(angle)); 
		vy[2] = (int)(y + 10f * Math.sin(angle)); 
		vx[3] = vx[0];
		vy[3] = vy[0];
		gr.setColor(tortue.getCouleur()); 
		gr.fillPolygon(vx,vy,4); 
		gr.setColor(Color.red); 
		gr.drawPolygon(vx, vy, 4);
	}
	
	/**
	 * Methode accesseur qui permet d'acceder a tortue, qui est la tortue sur laquelle on travaille
	 * @return La tortue
	 */
	
	public Tortue getTortue(){
		return this.tortue;
	}
	
	/**
	 * Methode qui insere le trace passe en parametre dans le tableau des traces
	 * @param t
	 *		Trace que l'on souhaite ajouter
	 */
	public void insererTrace(Trace t){
		tabTrace.add(t);
	}
	
}