import java.awt.*;
import java.util.Scanner;
import javax.swing.*;

/** Classe Principale */
public class Principale {
	
	public static void main(String[] args) {
		JFrame fenetre = new JFrame("Simple Logo");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SimpleLogo logo = new SimpleLogo();
		logo.setPreferredSize(new Dimension(400,400));
		fenetre.setContentPane(logo);
		fenetre.pack();
		fenetre.setVisible(true);
		logo.initialiserTortue();
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String s;
		fenetre.setResizable(false);
		while (true) {
			s=sc.nextLine();
			Dialogue.executerCommande(logo, s);
		}
	}
}
