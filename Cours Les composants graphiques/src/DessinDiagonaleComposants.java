import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
@SuppressWarnings("serial")
public class DessinDiagonaleComposants extends JPanel {
private Color couleur;
public DessinDiagonaleComposants() {
couleur=Color.red;
// bouton "Effacer"
JButton b=new JButton("Effacer"); b.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent e) { getGraphics().clearRect(0,0,getWidth(),getHeight());
} });
this.add(b);
// choix de la couleur de la diagonale
@SuppressWarnings({ "unchecked", "rawtypes" })
final JComboBox c=new JComboBox(new String[] {"rouge","vert","jaune","bleu"});
c.addActionListener(new ActionListener() {
public void actionPerformed(ActionEvent e) { 
	switch (c.getSelectedIndex()) {
case 0 :
couleur=Color.red;
break; 
case 1 :
couleur=Color.green;
break; 
case 2 :
couleur=Color.yellow;
break; 
	case 3 :
couleur=Color.blue;
break; 
default :
couleur=Color.black; }
repaint(); }
}); 
this.add(c);
}
public void paintComponent(Graphics g) {
super.paintComponent(g); int h=getHeight();
int w=getWidth(); 
g.setColor(couleur); 
g.drawLine(0,0,w-1,h-1);
} }