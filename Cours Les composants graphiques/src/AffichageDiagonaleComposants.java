import java.awt.Dimension;

import javax.swing.JFrame;

public class AffichageDiagonaleComposants { 
	public static void main(String[] args) {
		JFrame fenetre=new JFrame("Affichage d’une diagonale rouge"); 
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		DessinDiagonaleComposants dessin=new DessinDiagonaleComposants(); 
		dessin.setPreferredSize(new Dimension(600,480)); 
		fenetre.setContentPane(dessin);
		fenetre.pack(); fenetre.setVisible(true);
}
}