import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class CliqueMoi extends JPanel {
	

	private int posX,posY;
	private static int fenX;
	private static int fenY; 
	public static final int rayon=20;
	
	public CliqueMoi(int dimX, int dimY){
		this.setPreferredSize(new Dimension(dimX,dimY));
		CliqueMoi.fenX = dimX;
		CliqueMoi.fenY = dimY;
		this.posX=(int) (Math.random()*fenY%(fenX-rayon));
		this.posY=(int) (Math.random()*fenY%(fenY-rayon));
		MouseMotionListener n=new MouseMotionListener(){
			public void mouseMoved(MouseEvent e){
				deplacement(e.getX(),e.getY());
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		};
		this.addMouseMotionListener(n);
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillOval(posX, posY, rayon, rayon);
	}
		
	public static void main(String[] args){
		JFrame fen=new JFrame("Clique-moi");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		CliqueMoi n=new CliqueMoi(400,400);
		fen.setContentPane(n);
		fen.pack();
		fen.setVisible(true);
		fen.setResizable(false);
	}
	
	public void deplacement(int x, int y){
		if (this.distance(x,y)< 1.2*rayon){
			this.posX=(int) (Math.random()*fenX%(fenX-rayon));
			this.posY=(int) (Math.random()*fenY%(fenY-rayon));
			repaint();
		}
	}
	
	private int distance(int x, int y){
		int i, diffX, diffY;
		if (x<0){
			x=-x;
		}
		if (y<0){
			y=-y;
		}
		if (x<this.posX){
			diffX = this.posX-x;
		}
		else{
			diffX = x-this.posX;
		}
		if (x<this.posX){
			diffY = this.posY-y;
		}
		else{
			diffY = y-this.posY;
		}
		if (diffX<diffY){
			i=diffY-diffX;
		}
		else{
			i=diffX-diffY;
		}
		return i;
	}
}
