import javax.swing.*;
import java.awt.*;

public class Principale {
	public static void main(String[] args){
		JFrame fenetre=new JFrame("Dessine moi");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FenetrePrincipale pp=new FenetrePrincipale(new BorderLayout());
		Scribble s=new Scribble();
		ControleCouleur cc=new ControleCouleur(s);
		s.setPreferredSize(new Dimension(400,400));
		pp.add(cc,BorderLayout.NORTH);
		pp.add(s,BorderLayout.CENTER);
		fenetre.setContentPane(pp);
		fenetre.pack();
		fenetre.setVisible(true);
	}
}
