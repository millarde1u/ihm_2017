import java.awt.Color;

public class Trait {

	private int dX, dY, aX, aY;
	Color c;
	
	public Trait(int dX, int dY, int aX, int aY, Color c) {
		this.dX = dX;
		this.dY = dY;
		this.aX = aX;
		this.aY = aY;
		this.c = c;
	}

	public int getdX() {
		return dX;
	}

	public int getdY() {
		return dY;
	}

	public int getaX() {
		return aX;
	}

	public int getaY() {
		return aY;
	}

	public Color getC() {
		return c;
	}
	
	
}
