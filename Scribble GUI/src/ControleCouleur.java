import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class ControleCouleur extends JPanel{
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ControleCouleur(Scribble s){
		
		Border compound;
		Border raisedbevel=BorderFactory.createRaisedBevelBorder();
		Border lowbevel=BorderFactory.createLoweredBevelBorder();
		compound = BorderFactory.createCompoundBorder(raisedbevel,lowbevel);
		Border margin = new EmptyBorder(10,10,10,10);
		this.setBorder(compound);
		
		JButton b=new JButton("Effacer");
		b.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				s.effacer();
			}
		});
		this.add(b);
		JComboBox c=new JComboBox(new String[] {"Noir", "Bleu","Rouge", "Violet", "Jaune",});
		c.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (c.getSelectedIndex()){
				case 0 : 
					s.changerCouleur(Color.BLACK);
					break;
				case 1 :
					s.changerCouleur(Color.BLUE);
					break;
				case 2 : 
					s.changerCouleur(Color.RED);
					break;
				case 3 : 
					s.changerCouleur(new Color(150,0,150));
					break;
				case 4 : 
					s.changerCouleur(Color.yellow);
					break;
				}
				
			}
			
		});
		this.add(c);
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}
}