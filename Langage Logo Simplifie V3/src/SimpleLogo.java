import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SimpleLogo extends JPanel{
	
	private Tortue tortue;
	private ArrayList<Trace> tabTrace;
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		if(tortue.etreVisible()){
			this.placerTortue((int)tortue.getX(),(int)tortue.getY(),tortue.getAngle(),g);		
		}
		for (Trace n:tabTrace){
			if (!n.isLeve()){
				g.setColor(n.getCouleur());
				g.drawLine((int)n.getP1().getX(), (int)n.getP1().getY(), (int)n.getP2().getX(), (int)n.getP2().getY());
			}
		}
	}
	
	/**
	 * Methode qui permet d'initialiser les attributs
	 */
	
	public void initialiserTortue(){
		tortue=new Tortue(this.getWidth()/2,this.getHeight()/2,Color.black,1);
		this.tabTrace=new ArrayList<Trace>();
	}
	
	/**
	* Place la tortue sur l’écran à la position x,y et faisant un angle ang. * @param x abscisse de la position de la tortue
	* @param y ordonnée de la position de la tortue
	* @param ang orientation du crayon de la tortue
	* @param gr contexte graphique.
	*/
	public void placerTortue(int x, int y, int ang, Graphics gr){ 
	int vx[] = new int[4];
	int vy[] = new int[4];
	// Calculer la disposition des sommets de la 
	double angle = Math.toRadians(ang);
	vx[0] = (int)(x + 15f * Math.cos( angle )); 
	vy[0] = (int)(y + 15f * Math.sin( angle )); 
	angle += 2 * Math.PI / 3.0;
	vx[1] = (int)(x + 10f * Math.cos( angle )); 
	vy[1] = (int)(y + 10f * Math.sin( angle )); 
	angle += 2f * Math.PI / 3f;
	vx[2] = (int)(x + 10f * Math.cos( angle )); 
	vy[2] = (int)(y + 10f * Math.sin( angle )); 
	vx[3] = vx[0];
	vy[3] = vy[0];
	gr.setColor(tortue.getCouleur()); 
	gr.fillPolygon(vx,vy,4); 
	gr.setColor(Color.red); 
	gr.drawPolygon( vx, vy, 4 );
	}
	
	/**
	 * Methode qui permet d'acceder à la tortue sur laquelle on travaille
	 * 
	 * @return la fameuse torute
	 */
	
	public Tortue getTortue(){
		return this.tortue;
	}
	
	/**
	 * methode permettant d'inserer le dessin que l'on vient de faire
	 * 
	 * @param trace que l'on souhaite ajouter
	 */
	public void insererTrace(Trace t){
		tabTrace.add(t);
	}
	
}
