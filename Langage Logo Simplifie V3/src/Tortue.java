import java.awt.*;

public class Tortue {
	private double x;
	private double y;
	private int orientation;
	private Color couleur;
	private boolean leve;
	private boolean tAffiche;
	
	public Tortue(int x, int y, Color c, int e){
		this.x = x;
		this.y = y;
		this.couleur = c;
		this.orientation = -90;
		this.tAffiche=true;
	}
	
	public void afficherTortue(){
		this.tAffiche=true;
	}
	
	public void cacherTortue(){
		this.tAffiche=false;
	}
	
	public void baisserCrayon(){
		this.leve=false;
	}
	
	public void leverCrayon(){
		this.leve=true;
	}
	public boolean etreLeve(){
		return this.leve;
	}
	
	public boolean etreVisible(){
		return tAffiche;
	}
	
	public int getAngle(){
		return this.orientation;
	}
	public void setAngle(int angle){
		this.orientation = angle;
	}
	
	public Color getCouleur(){
		 return this.couleur;
	 }
	 
	 public void setCouleur(Color c){
		 this.couleur=c;
	 }
	 
	public double getX(){
		return this.x;
	}
	
	public double getY(){
		return this.y;
	}
	
	public void setPosition(double x, double y){
		this.x = x;
		this.y = y;
	}
}
