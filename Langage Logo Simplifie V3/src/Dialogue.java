import java.awt.*;
import java.util.*;

public class Dialogue {
	
	private static SimpleLogo logo;
	private static Tortue tortue;
	
	public Dialogue(){
		
	}
	
	private static void avancer(int a){
		double b=a*Math.cos(Math.toRadians(tortue.getAngle()))+tortue.getX();
		double c = a*Math.sin(Math.toRadians(tortue.getAngle()))+tortue.getY();
		Trace t=new Trace(tortue.getX(),tortue.getY(),b,c,tortue.getCouleur(),tortue.etreLeve());
		logo.insererTrace(t);
		tortue.setPosition(b, c);

		
	}
	
	private static void reculer(int a){
		Dialogue.avancer(-a);
	}
	
	private static void TournerDroite(int a){
		tortue.setAngle(tortue.getAngle()+a);
	}
	
	private static void TournerGauche(int a){
		tortue.setAngle(tortue.getAngle()-a);
	}
	
	private static void baisserCrayon(){
		tortue.baisserCrayon();
	}
	
	private static void leverCrayon(){
		tortue.leverCrayon();
	}
	
	private static void afficherTortue(){
		tortue.afficherTortue();
	}
	/*
	 * Méthode permettant de cacher la tortue.
	 */
	private static void cacherTortue(){
		tortue.cacherTortue();
	}
	
	/*
	 * Méthode permettant de changer la couleur des traits
	 */
	
	private static void changerCouleur(String c){
		if (c.equals("white") || c.equals("WHITE")){
			tortue.setCouleur(Color.WHITE);
		}
		if (c.equals("black") || c.equals("BLACK")){
			tortue.setCouleur(Color.BLACK);
		}
		if (c.equals("blue") || c.equals("BLUE")){
			tortue.setCouleur(Color.BLUE);
		}
		if (c.equals("red") || c.equals("RED")){
			tortue.setCouleur(Color.RED);
		}
		if (c.equals("green") || c.equals("GREEN")){
			tortue.setCouleur(Color.GREEN);
		}
	}
	private static void effacer(){
		logo.initialiserTortue();
	}
	/* 
	 * Méthosde qui permet d'executer la bonne m"thode en fonction du string rentré en paramètre
	 * @return indique si l'action a été effectuée ou non.
	 */
	public static boolean executerCommande(SimpleLogo l, String s){
		Dialogue.tortue = l.getTortue();
		Dialogue.logo = l;
		Scanner sc =new Scanner(s);
		String entree,argument;
		boolean etat=true;
		try{
			while(sc.hasNext()){
				entree=(sc.next()).toLowerCase();
				switch(entree){
				case "av" :
					argument = sc.next();
					Dialogue.avancer(Integer.parseInt(argument));
					l.repaint();
					break;
				case "re" :
					argument=sc.next();
					Dialogue.reculer(Integer.parseInt(argument));
					l.repaint();
					break;
				case "td" :
					argument=sc.next();
					Dialogue.TournerDroite(Integer.parseInt(argument));
					l.repaint();
					break;
				case "tg" :
					argument=sc.next();
					Dialogue.TournerGauche(Integer.parseInt(argument));
					l.repaint();
					break;
				case "lc" :
					Dialogue.leverCrayon();
					l.repaint();
					break;
				case "bc" :
					Dialogue.baisserCrayon();
					l.repaint();
					break;
				case "ct" :
					Dialogue.cacherTortue();
					l.repaint();
					break;
				case "mt" :
					Dialogue.afficherTortue();
					l.repaint();
					break;
				case "eff" :
					Dialogue.effacer();
					l.repaint();
					break;
				case "c" :
					argument=sc.next();
					Dialogue.changerCouleur(argument);
					l.repaint();
					break;
				default :
					etat = false;
				}
			}
		}
		catch(NumberFormatException e1){
			System.out.println("§§ Petite erreur de syntaxe §§");
		}
		catch(Exception e2){
			System.out.println("§§ Errueur inconnue §§");
			e2.printStackTrace();
		}
		sc.close();
		return etat;
	}
}
