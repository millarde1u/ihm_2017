import java.awt.*;

public class Trace {
	private Point p1;
	private Point p2;
	private Color couleur;
	private boolean leve;
	
	public Trace(double p1X,double p1Y, double p2X, double p2Y, Color c, boolean l){
		this.p1=new Point();
		p1.setLocation(p1X, p1Y);
		this.p2=new Point();
		p2.setLocation(p2X, p2Y);
		this.couleur=c;
		this.leve=l;
	}
	
	
	public Point getP1() {
		return p1;
	}
	public void setP1(Point p1) {
		this.p1 = p1;
	}
	public Point getP2() {
		return p2;
	}
	public void setP2(Point p2) {
		this.p2 = p2;
	}
	public Color getCouleur() {
		return couleur;
	}
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	public boolean isLeve() {
		return leve;
	}
	public void setLeve(boolean leve) {
		this.leve = leve;
	}
	
}
