import javax.swing.*;
import java.awt.*;

public class ControleCadreRouge {
	public static void main(String[] args) {
		JFrame fenetre=new JFrame("Affichage d’un cadre rouge"); 
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		DessinCadreRouge dessin=new DessinCadreRouge(1); 
		dessin.setPreferredSize(new Dimension(600,480)); 
		fenetre.setContentPane(dessin);
		fenetre.pack(); 
		fenetre.setVisible(true); 
		dessin.requestFocusInWindow();
	} 
}