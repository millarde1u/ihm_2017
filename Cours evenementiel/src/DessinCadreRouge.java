import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class DessinCadreRouge extends JPanel {
	private int epaisseur;
	public DessinCadreRouge(int ep) {
		epaisseur=ep;
		KeyListener kl=new KeyListener() {
			public void keyTyped(KeyEvent e) {
				
			}
			public void keyReleased(KeyEvent e) { }
			public void keyPressed(KeyEvent e) {
				char c=e.getKeyChar();
				if(c=='+') {
					setEpaisseur(epaisseur+1); 
				} 
				else if(c=='-') {
					if(epaisseur>1) { 
						setEpaisseur(epaisseur-1);
					}
				}
			}
		};
		this.addKeyListener(kl); 
	}
	public void paintComponent(Graphics g) { 
		super.paintComponent(g); 
		g.setColor(Color.red);
		int h=getHeight();
		int w=getWidth();
		for(int i=2;i<2+epaisseur;i++){
			g.drawRect(i,i,w-2*i-1,h-2*i-1); 
		}
	}
	
	public void setEpaisseur(int e) { 
		epaisseur=e;
		repaint();
	} 
}
