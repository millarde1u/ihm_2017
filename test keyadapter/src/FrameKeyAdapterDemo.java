import java.awt.*;

@SuppressWarnings("serial")
public class FrameKeyAdapterDemo extends Frame
{
  public FrameKeyAdapterDemo()
  {
    Helper h1 = new Helper();
    addKeyListener(h1);

    setSize(300, 300);
    setVisible(true);
  }

  public static void main(String args[])
  {
    new FrameKeyAdapterDemo();    
  }
}