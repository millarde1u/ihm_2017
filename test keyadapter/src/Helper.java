import java.awt.event.*;

class Helper extends KeyAdapter
{
  public void keyTyped(KeyEvent e)
  {
    char char1 = e.getKeyChar();
    System.out.println("You typed " + char1 + " character from keyboard");
  }
}