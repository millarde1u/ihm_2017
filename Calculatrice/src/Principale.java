import java.awt.BorderLayout;
import javax.swing.JFrame;

public class Principale {
	public static void main(String[] args){
		JFrame fenetre=new JFrame("Dessine moi");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FenetrePrincipale fond=new FenetrePrincipale(new BorderLayout());
		Changement c=new Changement();
		Ecran ec=new Ecran(c);
		Boutons bouton=new Boutons(c,ec);
		fond.add(ec, BorderLayout.NORTH);
		fond.add(bouton,BorderLayout.SOUTH);
		fenetre.setContentPane(fond);
		fenetre.setSize(400, 400);
		fenetre.setVisible(true);
		
		fenetre.pack();
	}
}
