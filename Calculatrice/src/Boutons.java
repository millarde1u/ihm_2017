import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Boutons extends JPanel{
	
	private Changement chg;
	private Ecran ec;
	private int indice;
	private JButton[] liste_bouton={new JButton("7"),new JButton("8"), new JButton("9"), new JButton("+")
			,new JButton("4"), new JButton("5"), new JButton("6"), new JButton("-")
			,new JButton("1"), new JButton("2"), new JButton("3"), new JButton("*")
			,new JButton("0"), new JButton("Clr"), new JButton("x2"), new JButton("/")
			,new JButton(""), new JButton("."), new JButton("sqrt"), new JButton("=")};;
	
	public Boutons(Changement g,Ecran e){
		Panel p=new Panel();
		p.setLayout(new GridLayout(5,4));
		this.chg=g;
		this.ec=e;
		for(indice=0; indice<liste_bouton.length; indice++){
			liste_bouton[indice].setActionCommand(liste_bouton[indice].getName());
			liste_bouton[indice].addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					switch(((JButton)e.getSource()).getActionCommand()){
					case "1":
						chg.nombre(1.0);
						break;
					case "2":
						chg.nombre(2.0);
						break;
					case "3":
						chg.nombre(3.0);
						break;
					case "4":
						chg.nombre(4.0);
						break;
					case "5":
						chg.nombre(5.0);
						break;
					case "6":
						chg.nombre(6.0);
						break;
					case "7":
						chg.nombre(7.0);
						break;
					case "8":
						chg.nombre(8.0);
						break;
					case "9":
						chg.nombre(9.0);
						break;
					case "0":
						chg.nombre(0.0);
						break;
					default : 
						break;
					}
					ec.repaint();
					repaint();
					
				}
			});
			p.add(liste_bouton[indice]);
		}
		this.add(p);
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}
	
}
