
public class Changement {
	
	private double affichage, retenue;
	public boolean addition, soustraction, multiplication, division,premier;
	
	public Changement(){
		this.affichage=0.0;
		addition = false;
		soustraction = false;
		multiplication = false;
		division=false;
		premier=true;
	}
	
	public String getAffichage(){
		return Double.toString(affichage);
	}
	
	public void nombre(Double a){
		this.affichage*=10;
		this.affichage=this.affichage + a;
	}
	
	public void addition(){
		this.egale();
		this.retenue = this.affichage;
		this.affichage=0.0;
		this.affichage=0.0;
		addition = true;
		soustraction = false;
		multiplication = false;
		division=false;
		premier=false;
	}
	
	public void soustraction(){
		this.egale();
		this.retenue = this.affichage;
		this.affichage=0.0;
		this.affichage=0.0;
		addition = false;
		soustraction = true;
		multiplication = false;
		division=false;
		premier=false;
	}
	
	public void multiplication(){
		this.egale();
		this.retenue = this.affichage;
		this.affichage=0.0;
		this.affichage=0.0;
		addition = false;
		soustraction = true;
		multiplication = false;
		division=false;
		premier=false;
	}
	
	public void division(){
		this.egale();
		this.retenue = this.affichage;
		this.affichage=0.0;
		this.affichage=0.0;
		addition = false;
		soustraction = false;
		multiplication = false;
		division=true;
		premier=false;
	}
	
	public void egale(){
		if (addition){
			this.affichage=affichage+retenue;
		}
		if (soustraction && !premier){
			this.affichage=affichage-retenue;
		}
		if (multiplication && !premier){
			this.affichage=affichage*retenue;
		}
		if (division && !premier && affichage != 0){
			this.affichage=retenue/affichage;
		}
	}
}
