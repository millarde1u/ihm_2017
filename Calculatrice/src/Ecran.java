import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Ecran extends JPanel{
	
	private Changement chg;
	private JTextField texte;
	
	public Ecran(Changement g){
		this.chg=g;
		texte=new JTextField(chg.getAffichage());
		this.add(texte);
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		texte.setText(chg.getAffichage());
	}
	
}
