import java.awt.*;
import javax.swing.*;
public class ExempleBorderLayout {
    public static void main(String[] args) {
        JPanel jp=new JPanel();
        jp.setLayout(new BorderLayout());
        jp.add(new JButton("North"), BorderLayout.NORTH);
        jp.add(new JButton("South"), BorderLayout.SOUTH);
        jp.add(new JButton("East"), BorderLayout.EAST);
        jp.add(new JButton("West"), BorderLayout.WEST);
        jp.add(new JButton("Center"), BorderLayout.CENTER);
        JFrame f=new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jp.setPreferredSize(new Dimension(600,480));
        f.setContentPane(jp);
        f.pack();
        f.setVisible(true);
    }
}