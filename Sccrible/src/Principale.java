import javax.swing.*;
import java.awt.*;

public class Principale {
	public static void main(String[] args){
		JFrame fenetre=new JFrame("Dessine moi");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Scribble s=new Scribble();
		s.setPreferredSize(new Dimension(400,400));
		fenetre.setContentPane(s);
		fenetre.pack();
		fenetre.setVisible(true);
		s.requestFocusInWindow();
		
		
	}
}
