import java.awt.Color;
import java.awt.event.*;

public class ControleCouleur extends KeyAdapter {
	
	public static boolean m=false;
	public Scribble g;
	
	ControleCouleur(Scribble h){
		this.g=h;
	}
	@Override
	public void keyPressed(KeyEvent e){
		switch(e.getKeyChar()){
		case 'r' :
			g.changerCouleur(Color.RED);
			m=false;
			break;
		case 'v' : 
			g.changerCouleur(Color.green);
			m=false;
			break;
		case 'b' : 
			g.changerCouleur(Color.blue);
			m=false;
			break;
		case 'j' :
			g.changerCouleur(Color.yellow);
			m=false;
			break;
		case 'g' :
			g.changerCouleur(Color.green);
			m=false;
		case 'm' :
			m=true;
			break;
		case '&' :
			if (m){
				g.changerCouleur(new Color(170,0,170));
			}
			m=false;
			break;
		case 'z' :
			g.changerCouleur(Color.pink);
			m=false;
			break;
		case 'n' : 
			g.changerCouleur(Color.BLACK);
			m=false;
			break;
		default :
			g.changerCouleur(Color.WHITE);
			m=false;
			break;
		} 
	}
}
