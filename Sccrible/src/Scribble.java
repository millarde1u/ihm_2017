import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.awt.event.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Scribble extends JPanel{
	
	
	private int posX, posY;
	private Color c = Color.RED;
	private List<Trait> tabTrace;
	
	public Scribble(){
		
	MouseMotionListener m=new MouseMotionListener(){
		@Override
		public void mouseDragged(MouseEvent e) {
			if (SwingUtilities.isLeftMouseButton(e)){
				//Graphics g = getGraphics();
				//g.setColor(c);
				//g.drawLine(posX,posY,e.getX(), e.getY());
				ajoutTrait(posX, posY, e.getX(),e.getY());
				posX=e.getX();
				posY=e.getY();
				repaint();
			}
		}

		@Override
		public void mouseMoved(MouseEvent e) {}
	};
		
	MouseListener n=new MouseListener(){
		@Override
		public void mouseClicked(MouseEvent e) {}

		@Override
		public void mousePressed(MouseEvent e) {
			if (SwingUtilities.isLeftMouseButton(e)){
				posX = e.getX();
				posY = e.getY();
			}
			if (SwingUtilities.isMiddleMouseButton(e)){
				effacer();
				repaint();
			}
			if (SwingUtilities.isRightMouseButton(e)){
				removeMouseMotionListener(m);
				removeMouseListener(this);
				effacer();
				repaint();
			}
		
		}

		@Override
		public void mouseReleased(MouseEvent e) {}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
	};
	this.addKeyListener(new ControleCouleur(this));
	this.addMouseListener(n);
	this.addMouseMotionListener(m);
	tabTrace = new ArrayList<Trait>();
}
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		for (Trait n:tabTrace){
			g.setColor(n.getC());
			g.drawLine(n.getdX(), n.getdY(), n.getaX(), n.getaY());
		}
	}
	
	public void changerCouleur(Color col){
		if (c != null){
			this.c=col;
		}
	}
	
	public void ajoutTrait(int dX, int dY, int aX, int aY){
		tabTrace.add(new Trait(dX, dY, aX, aY, c));
	}
	
	public void effacer(){
		List<Trait> a=new ArrayList<Trait>();
		this.tabTrace=a;
	}
}
