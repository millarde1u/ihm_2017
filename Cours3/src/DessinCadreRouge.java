import javax.swing.*; 
import java.awt.*; 

public class DessinCadreRouge extends JPanel { 
	private int epaisseur;
	public DessinCadreRouge(int e) {
		epaisseur=e; 
	}
	public void paintComponent(Graphics g) { 
		super.paintComponent(g); 
		g.setColor(Color.red);
		int h=getHeight();
		int w=getWidth();
		for(int i=2;i<2+epaisseur;i++)
		g.drawRect(i,i,w-2*i-1,h-2*i-1);
	}
	public void setEpaisseur(int e) { 
		epaisseur=e;
		repaint();
	}
}