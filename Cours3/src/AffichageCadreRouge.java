import javax.swing.*; 
import java.awt.*; 
import java.util.*;


public class AffichageCadreRouge {
	public static void main(String[] args) {
		int e=1;
		Scanner scan;
		scan = new Scanner(System.in);
		JFrame fenetre=new JFrame("Affichage d’un cadre rouge"); 
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		DessinCadreRouge dessin=new DessinCadreRouge(e); 
		dessin.setPreferredSize(new Dimension(600,480)); 
		fenetre.setContentPane(dessin);
		fenetre.pack();
		fenetre.setVisible(true);
		while (true) {
			System.out.print("Epaisseur : "); e = scan.nextInt() ;
			if(e>0) {
				dessin.setEpaisseur(e); 
			}
		}		
	}
}