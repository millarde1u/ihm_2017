
import java.awt.*;
import javax.swing.*;
public class ExempleGridLayout {
    public static void main(String[] args) {
        JPanel jp=new JPanel();
        jp.setLayout(new GridLayout(3,2));
        jp.add(new JButton("un"));
        jp.add(new JButton("deux"));
        jp.add(new JButton("trois"));
        jp.add(new JButton("quatre"));
        jp.add(new JButton("cinq"));
        JFrame f=new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jp.setPreferredSize(new Dimension(400,250));
        f.setContentPane(jp);
        f.pack();
        f.setVisible(true);
    }
}