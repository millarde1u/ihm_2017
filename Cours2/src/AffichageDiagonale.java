import javax.swing.*;
import java.awt.*;

public class AffichageDiagonale {
	public static void main(String[] args) {
		JFrame fenetre=new JFrame("Affichage d’une diagonale rouge"); 
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		DessinDiagonale dessin=new DessinDiagonale(); 
		dessin.setPreferredSize(new Dimension(600,480)); 
		fenetre.setContentPane(dessin);
		fenetre.pack();
		fenetre.setVisible(true);
	}
}