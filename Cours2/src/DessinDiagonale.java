import javax.swing.*;
import java.awt.*;

public class DessinDiagonale extends JPanel {
	public void paintComponent(Graphics g) { 
		super.paintComponent(g);
		int h=this.getHeight();
		int w=this.getWidth();
		g.setColor(Color.BLUE); 
		g.drawLine(0,0,w-1,h-1);
	}
}