import java.awt.*;
import javax.swing.*;

public class SimpleLogo extends JPanel{
	
	private Tortue tortue;
	private Trace[] tabTrace;
	private int indiceTrace;
	private static int MAXTAILLE=1000;
	
	@Override
	public void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		if(tortue.etreVisible()){
			this.placerTortue((int)tortue.getX(),(int)tortue.getY(),g);
			
		}
	}
	
	public void initialiserTortue(){
		tortue=new Tortue(this.getWidth()/2,this.getHeight()/2,Color.black,1);
	}
	
	public void placerTortue(int x, int y, Graphics gr){
		gr.setColor(Color.RED);
		int d=10;
		int xpets[]={x-d, x+d, x};
		int ypets[]={y, y, y-2*d};
		gr.drawPolygon(xpets, ypets, 3);
	}
	

	
	public Tortue getTortue(){
		return this.tortue;
	}
	
	
}
