
import javax.swing.*; 
import java.awt.*;
import java.awt.event.*; 
public class SuiviSouris {
	public static void afficheEvenement(String pos,MouseEvent e) { 
		System.out.print(pos+" : ("+e.getX()+","+e.getY()+")"); 
		if(SwingUtilities.isLeftMouseButton(e)) {	
			System.out.println(" bouton de gauche");
		} 
		else if(SwingUtilities.isMiddleMouseButton(e)) {
			System.out.println(" bouton du milieu");
		} 
		else if(SwingUtilities.isRightMouseButton(e)) {
			System.out.println(" bouton de droite");
		} 
		else 
			System.out.println();
		}
public static void main(String[] args) {
	JFrame fen=new JFrame("Exemple simple de traitement des evenements souris");
	fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	JPanel contenant=(JPanel)(fen.getContentPane()); contenant.setPreferredSize(new Dimension(200,250));
	fen.pack();
	fen.setVisible(true);
// creation de l’auditeur anonyme 
	MouseListener ml=new MouseListener() {
		public void mouseClicked(MouseEvent e) { 
			afficheEvenement("clicked",e);
		}
		public void mouseEntered(MouseEvent e) {
			afficheEvenement("entered",e); 
		}
		public void mouseExited(MouseEvent e) { 
			afficheEvenement("exited",e);
		}
		public void mousePressed(MouseEvent e) {
			afficheEvenement("pressed",e); 
		}
		public void mouseReleased(MouseEvent e) { 
			afficheEvenement("released",e);
		}
	};
	MouseMotionListener mml=new MouseMotionListener() { 
		public void mouseMoved(MouseEvent e) {
			afficheEvenement("moved",e); 
		}
		public void mouseDragged(MouseEvent e) { 
			afficheEvenement("dragged",e);
		} 
	};
	// association source-auditeurs
	contenant.addMouseListener(ml);
	contenant.addMouseMotionListener(mml); 
	}
}
