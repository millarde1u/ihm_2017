package modele;

import java.awt.Color;
import java.awt.Graphics;

public abstract class FigureColoree {

	private static final int TAILLE_CARRE_SELECTION=4;
	
	private boolean selected;
	
	protected Color couleur;
	
	public FigureColoree() {
		
	}
	
	public abstract int nbPoints();
	
	public abstract int nbClics();
	
	public abstract void modifierPoints(Point[] tp);
	
	public void affiche(Graphics g) {
		
	}
	
	public void selectionne() {
		
	}
	
	public void deSelectionne() {
		
	}
	
	public void changeCouleur(Color coul) {
		
	}
	
}