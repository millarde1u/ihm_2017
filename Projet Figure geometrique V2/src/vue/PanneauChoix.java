package vue;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class PanneauChoix extends JPanel{
	
	private DessinFigures dessin;
	
	public PanneauChoix(DessinFigures d){
		this.dessin = d;
		ButtonGroup group=new ButtonGroup();
		JRadioButton nouvFig=new JRadioButton("Nouvelle figure");
		nouvFig.setSelected(true);
		JRadioButton mainLeve=new JRadioButton("Tracé à main levée");
		JRadioButton manipulation=new JRadioButton("Manipulations");
		JComboBox c=new JComboBox(new String[] {"Carre","Triangle","Quadrilatere"});
		c.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){} 
		});
		JComboBox d1=new JComboBox(new String[] {"Noir", "Bleu","Rouge", "Violet", "Jaune",});
		d1.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e){} 
		});
		group.add(nouvFig);
		group.add(mainLeve);
		group.add(manipulation);
		this.add(nouvFig);
		this.add(mainLeve);
		this.add(manipulation);
		this.add(c);
		this.add(d1);
	}
	
	private Color determineCouleur(int index){
		/*Color c;
		switch(index){
		case 1 : 
			break;
		}*/Color c=Color.black;
		return c;
		
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}
}
