

/**
 * classe de etst pour le TD4
 * m�thode sans r�sultat
 * 
 * @author vthomas
 * 
 */
public class Test2MethodeRotation {

	/**
	 * fait un test de rotation
	 */
	public void test_7_Rotation() {
		UnRectangle r = new UnRectangle(
				new UnPoint(10, 20), 100, 200);
		r.faireRotation();
		Lanceur.assertEquals(-190, r.coin.abscisse,
				"mauvaise abscisse du coin");
		Lanceur.assertEquals(20, r.coin.ordonnee,
				"mauvaise ordonee du coin");
		Lanceur.assertEquals(100, r.hauteur,
				"mauvaise hauteur du rectangle");
		Lanceur.assertEquals(200, r.largeur,
				"mauvaise largeur du rectangle");		
	}
	
	
	
	/**
	 * lancement des tests
	 * 
	 * @param args
	 *            vide
	 */
	public static void main(String[] args) {
		Lanceur.lanceAvecInterface(new Test2MethodeRotation());
	}

}
