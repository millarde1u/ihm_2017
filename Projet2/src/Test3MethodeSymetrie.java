

/**
 * classe de etst pour le TD4
 * m�thode sans r�sultat
 * 
 * @author vthomas
 * 
 */
public class Test3MethodeSymetrie {

		
	
	/**
	 * fait un test de sym�trie du point
	 */
	public void test_8_SymetriePoint() {
		UnPoint p=new UnPoint(10, 20);
		p.faireSymetrie();
		Lanceur.assertEquals(20, p.abscisse,
				"mauvaise abscisse du point symetrique");
		Lanceur.assertEquals(10, p.ordonnee,
				"mauvaise ordonnee du point symetrique");	
	}
	
	/**
	 * fait un test de sym�trie
	 */
	public void test_9_Symetrie() {
		UnRectangle r = new UnRectangle(
				new UnPoint(10, 20), 100, 200);
		r.faireSymetrie();
		Lanceur.assertEquals(-180, r.coin.abscisse,
				"mauvaise abscisse du coin");
		Lanceur.assertEquals(110, r.coin.ordonnee,
				"mauvaise ordonee du coin");
		Lanceur.assertEquals(100, r.hauteur,
				"mauvaise hauteur du rectangle");
		Lanceur.assertEquals(200, r.largeur,
				"mauvaise largeur du rectangle");		
	}

	/**
	 * lancement des tests
	 * 
	 * @param args
	 *            vide
	 */
	public static void main(String[] args) {
		Lanceur.lanceAvecInterface(new Test3MethodeSymetrie());
	}

}
