/**
 * classe UnRectangle
 * mod�lise des rectangles
 */
class UnRectangle {

	/**
	 * attribut coin bas-gauche du rectangle
	 */
	UnPoint coin;

	/**
	 * attributs dimension du rectangle
	 */
	int largeur, hauteur;

	/**
	 * constructeur vide - rectangle unitaire (TD3)
	 */
	UnRectangle() {
		this.coin = new UnPoint();
		this.largeur = 1;
		this.hauteur = 1;
	}
	
	
	/**
	 * constructeur de rectangle � partir d'un point et des dimensions
	 * (TD3)
	 * 
	 * @param cig
	 *            coin inferieur gauche
	 * @param l
	 *            largeur
	 * @param h
	 *            hauteur
	 */
	UnRectangle(UnPoint cig, int l, int h) {
		if (cig == null)
			this.coin = new UnPoint();
		else
			this.coin = cig;
		if (l < 1)
			this.largeur = 1;
		else
			this.largeur = l;
		if (h < 1)
			this.hauteur = 1;
		else
			this.hauteur = h;
	}
	
	
	/**
	 * constructeur de rectangle a partir de 4 entiers
	 * 
	 * @param x
	 *            abscisse coin bas gauche
	 * @param y
	 *            abscisse coin bas droite
	 * @param l
	 *            largeur du rectangle
	 * @param h
	 *            hauteur du rectangle
	 */
	UnRectangle(int x, int y, int l, int h) {
		this.coin = new UnPoint(x, y);
		if (l < 1)
			this.largeur = 1;
		else
			this.largeur = l;
		if (h < 1)
			this.hauteur = 1;
		else
			this.hauteur = h;
	}
	
	/**
	 * constructeur par copie de rectangle
	 * (mis � l'origine si reference nulle)
	 * 
	 * @param r
	 *            rectangle � copier
	 */
	UnRectangle(UnRectangle r) {
		if (r != null) {
			this.coin = new UnPoint(r.coin);
			this.largeur = r.largeur;
			this.hauteur = r.hauteur;
		} else {
			this.coin = new UnPoint();
			this.largeur = 1;
			this.hauteur = 1;
		}
	}


	/**
	 * m�thode qui demande translation du rectangle
	 * cela revient � translater son coin
	 * 
	 * @param dx
	 *            variation selon axe abscisse
	 * @param dy
	 *            variation selon ax des ordonn�es
	 */
	void seTranslater(int dx, int dy) {
		this.coin.seTranslater(dx, dy);
	}

	/**
	 * methode chargee de modifier la largeur du rectangle
	 * 
	 * @param dl
	 *            variation de largeur
	 */
	void modifierLargeur(int dl) {
		this.largeur = this.largeur + dl;
		if (this.largeur < 1)
			this.largeur = 1;
	}

	/**
	 * methode chargee de modifier la hauteur du rectangle
	 * 
	 * @param dh
	 *            variation de hauteur
	 */
	void modifierHauteur(int dh) {
		this.hauteur = this.hauteur + dh;
		if (this.hauteur < 1)
			this.hauteur = 1;

	}

	/**
	 * demande au rectangle de faire une rotation selon coin bas
	 * gauche
	 */
	void faireRotation() {
		this.coin.seTranslater(-this.hauteur, 0);
		int aux = this.largeur;
		this.largeur = this.hauteur;
		this.hauteur = aux;
	}

	/**
	 * demande au rectangle de faire une sym�trie selon y=x
	 */
	void faireSymetrie() {
		// on obtient le coin sup�rieur droit
		this.coin.faireSymetrie();
		int aux = this.largeur;
		this.largeur = this.hauteur;
		this.hauteur = aux;
		this.coin.seTranslater(-this.largeur, this.hauteur);
	}

}
