

/**
 * classe de etst pour le TD4
 * m�thode sans r�sultat
 * 
 * @author vthomas
 * 
 */
public class TestMethodeRectangle {

	/**
	 * modifie largeur quand tout va bien
	 */
	public void test_1_ModifieLargeurNormal() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.largeur,
				"pas la bonne largeur initiale");
		r.modifierLargeur(1);
		Lanceur.assertEquals(2, r.largeur,
				"ne modifie pas la largeur");
	}

	/**
	 * modifie largeur quand elle devient n�gative
	 */
	public void test_2_ModifieLargeurNegative() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.largeur,
				"pas la bonne largeur initiale");
		r.modifierLargeur(-12);
		Lanceur.assertEquals(1, r.largeur,
				"la largeur ne peut pas etre n�gative");
	}

	/**
	 * modifie largeur quand elle devient egal � 0
	 */
	public void test_3_ModifieLargeurNulle() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.largeur,
				"pas la bonne largeur initiale");
		r.modifierLargeur(-1);
		Lanceur.assertEquals(1, r.largeur,
				"la largeur ne peut pas etre nulle");
	}

	/**
	 * modifie hauteur quand tout va bien
	 */
	public void test_4_ModifieHauteurNormal() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.hauteur,
				"pas la bonne hauteur initiale");
		r.modifierHauteur(1);
		Lanceur.assertEquals(2, r.hauteur,
				"ne modifie pas la hauteur");
	}

	/**
	 * modifie largeur quand elle devient n�gative
	 */
	public void test_5_ModifieHauteurNegative() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.hauteur,
				"pas la bonne hauteur initiale");
		r.modifierHauteur(-12);
		Lanceur.assertEquals(1, r.hauteur,
				"la hauteur ne peut pas etre n�gative");
	}

	/**
	 * modifie largeur quand elle devient egal � 0
	 */
	public void test_6_ModifieHauteurNulle() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.hauteur,
				"pas la bonne hauteur initiale");
		r.modifierLargeur(-1);
		Lanceur.assertEquals(1, r.hauteur,
				"la hauteur ne peut pas etre nulle");
	}

	/**
	 * fait un test de rotation
	 */
	public void test_7_Rotation() {
		UnRectangle r = new UnRectangle(
				new UnPoint(10, 20), 100, 200);
		r.faireRotation();
		Lanceur.assertEquals(-190, r.coin.abscisse,
				"mauvaise abscisse du coin");
		Lanceur.assertEquals(20, r.coin.ordonnee,
				"mauvaise ordonee du coin");
		Lanceur.assertEquals(100, r.hauteur,
				"mauvaise hauteur du rectangle");
		Lanceur.assertEquals(200, r.largeur,
				"mauvaise largeur du rectangle");		
	}
	
	
	/**
	 * fait un test de sym�trie du point
	 */
	public void test_8_SymetriePoint() {
		UnPoint p=new UnPoint(10, 20);
		p.faireSymetrie();
		Lanceur.assertEquals(20, p.abscisse,
				"mauvaise abscisse du point symetrique");
		Lanceur.assertEquals(10, p.ordonnee,
				"mauvaise ordonnee du point symetrique");	
	}
	
	/**
	 * fait un test de sym�trie
	 */
	public void test_9_Symetrie() {
		UnRectangle r = new UnRectangle(
				new UnPoint(10, 20), 100, 200);
		r.faireSymetrie();
		Lanceur.assertEquals(-180, r.coin.abscisse,
				"mauvaise abscisse du coin");
		Lanceur.assertEquals(110, r.coin.ordonnee,
				"mauvaise ordonee du coin");
		Lanceur.assertEquals(100, r.hauteur,
				"mauvaise hauteur du rectangle");
		Lanceur.assertEquals(200, r.largeur,
				"mauvaise largeur du rectangle");		
	}

	/**
	 * lancement des tests
	 * 
	 * @param args
	 *            vide
	 */
	public static void main(String[] args) {
		Lanceur.lanceAvecInterface(new TestMethodeRectangle());
	}

}
