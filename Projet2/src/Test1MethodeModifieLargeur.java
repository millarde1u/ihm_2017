

/**
 * classe de etst pour le TD4
 * m�thode sans r�sultat
 * 
 * @author vthomas
 * 
 */
public class Test1MethodeModifieLargeur {

	/**
	 * modifie largeur quand tout va bien
	 */
	public void test_1_ModifieLargeurNormal() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.largeur,
				"pas la bonne largeur initiale");
		r.modifierLargeur(1);
		Lanceur.assertEquals(2, r.largeur,
				"ne modifie pas la largeur");
	}

	/**
	 * modifie largeur quand elle devient n�gative
	 */
	public void test_2_ModifieLargeurNegative() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.largeur,
				"pas la bonne largeur initiale");
		r.modifierLargeur(-12);
		Lanceur.assertEquals(1, r.largeur,
				"la largeur ne peut pas etre n�gative");
	}

	/**
	 * modifie largeur quand elle devient egal � 0
	 */
	public void test_3_ModifieLargeurNulle() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.largeur,
				"pas la bonne largeur initiale");
		r.modifierLargeur(-1);
		Lanceur.assertEquals(1, r.largeur,
				"la largeur ne peut pas etre nulle");
	}

	/**
	 * modifie hauteur quand tout va bien
	 */
	public void test_4_ModifieHauteurNormal() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.hauteur,
				"pas la bonne hauteur initiale");
		r.modifierHauteur(1);
		Lanceur.assertEquals(2, r.hauteur,
				"ne modifie pas la hauteur");
	}

	/**
	 * modifie largeur quand elle devient n�gative
	 */
	public void test_5_ModifieHauteurNegative() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.hauteur,
				"pas la bonne hauteur initiale");
		r.modifierHauteur(-12);
		Lanceur.assertEquals(1, r.hauteur,
				"la hauteur ne peut pas etre n�gative");
	}

	/**
	 * modifie largeur quand elle devient egal � 0
	 */
	public void test_6_ModifieHauteurNulle() {
		UnRectangle r = new UnRectangle();
		Lanceur.assertEquals(1, r.hauteur,
				"pas la bonne hauteur initiale");
		r.modifierLargeur(-1);
		Lanceur.assertEquals(1, r.hauteur,
				"la hauteur ne peut pas etre nulle");
	}

	
	/**
	 * lancement des tests
	 * 
	 * @param args
	 *            vide
	 */
	public static void main(String[] args) {
		Lanceur.lanceAvecInterface(new Test1MethodeModifieLargeur());
	}

}
