import java.awt.*;
import javax.swing.*;

public class DeplacerCarrer {

	public static void main(String[] args) {
		JFrame fen=new JFrame("Tentative de deplacement du carre");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Carre ca=new Carre(200,200);
		ca.setPreferredSize(new Dimension (400,400));
		fen.setContentPane(ca);
		fen.pack();
		fen.setVisible(true);
		fen.setResizable(false);
		ca.requestFocusInWindow();

	}

}
