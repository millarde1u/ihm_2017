import java.awt.event.*;

public class ControleMouvement extends KeyAdapter{
		private Carre c;
		
		public ControleMouvement(Carre c){
		 this.c = c;
		}
		
		public void keyPressed(KeyEvent e){
			c.mouvement(e.getKeyCode());
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method 
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
}
