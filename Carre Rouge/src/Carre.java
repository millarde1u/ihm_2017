import java.awt.*;
import java.awt.event.KeyEvent;

import javax.swing.*;

@SuppressWarnings("serial")
public class Carre extends JPanel{
	
	private static final int cote=20;
	private int posX,posY;
	
	public Carre(int dimX, int dimY){
		this.setPreferredSize(new Dimension(dimX,dimY));
		this.posX = dimX/2;
		this.posY = dimY/2;
		this.addKeyListener(new ControleMouvement(this));
		
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		g.fillRect(1, 1, 400, 400);
		g.setColor(Color.RED);
		g.fillRect(this.posX, this.posY, Carre.cote, Carre.cote);
	}
	
	public void mouvement(int a){
		switch (a){
		case KeyEvent.VK_LEFT :
			if (posX>0)
			this.posX-=10;
			repaint();
			break;
		case KeyEvent.VK_DOWN:
			if(posY<400-Carre.cote)
			this.posY+=10;
			repaint();
			break;
		case KeyEvent.VK_UP : 
			if (posY>0)
			this.posY-=10;
			repaint();
			break;
		case KeyEvent.VK_RIGHT : 
			if (posX<400-Carre.cote)
			this.posX +=10;
			repaint();
			break;
		}
	}
}
